# GeoKiff 

### Sommaire :

1. [Présentation du projet](#présentation-du-projetindividuel)
2. [Technologies utilisées](#technologies-utilisées)
3. [Interets](#interets)
4. [Installation et utilisation](#installation-et-utilisation)
5. [Points forts](#points-forts)
6. [La V2](#pour-la-v2)
7. [Aperçu en image](#aperçu)
8. [Exemple de code utilisé](#exemple-de-code-utilisé)
9. [Difficultés](#difficultés)
10. [Ce que j'ai apprécié](#ce-que-jai-apprécié)
11. [License](#license)


### <span name="présentation-du-projetindividuel">Présentation du projet(individuel)</span>
> L'idée était de donner envie d'apprendre la géographie à des enfants.<br>
> Pour cela, j'ai décidé de faire une carte d'Europe intéractive.<br>En passant sur un pays, les informations principales de celui-ci s'affichent.

__*Temps*__
4 jours



## <span name="technologies-utilisées">Technologies utilisées</span>

* HTML5
* CSS3
* JavaScript 
    * Axios
    * API countries



## <span name="interets">Interets</span>

Le but de ce projet était de nous familiariser avec axios en faisant des requête HTTP via une API REST.<br>
Tout en stimulant notre créativité. 



## <span name="installation-et-utilisation">Installation et utilisation</span>

1. __Cloner le projet__ 
2. Installer les dépendances avec __npm install__



## <span name="points-forts">Points forts</span>

* Facile d'utilisation
* Les informations sont pertinantes.



## <span name="pour-la-v2">Pour la v2</span>

* Une carte responsive(pour des soucis de création des zones dynamiques, la carte actuelle n'est pas responsive).
* Pays manquants
* Manque de précision au passage de la souris
* Autres continents ?




## <span name="aperçu">Aperçu</span>

<img src="images/geokiff.png">



## <span name="exemple-de-code-utilisé">Exemple de code utilisé</span>

```javascript
    const axios = require('axios');

    axios.get("https://restcountries.eu/rest/v2/name/" + country)

        .then(function (response) {
            let div = document.createElement('div');
            div.className = "m-0 p-3 divContain";
            let title = document.createElement('h5');
            title.className = "card-title";
            let ul = document.createElement('ul');
            ul.className = "card-body";
            let liCapital = document.createElement('li');
            let liPopulation = document.createElement('li');
            let liMoney = document.createElement('li');
            let liFlag = document.createElement('li');
            let imgFlag = document.createElement('img');
            imgFlag.className = "imgFlag";

            // ... (suite dans src/functions.js)
```

Utilisation d'une API REST avec axios + manipulation du DOM.<br>
[Voir le code en entier](https://gitlab.com/mel.gvn/geokiff/blob/master/src/functions.js)



## <span name="difficultés">Difficultés</span> 

La création des zones dynamiques pour l'event au passage de la souris(à améliorer).



## <span name="ce-que-jai-apprécié">Ce que j'ai apprécié</span>

Manipuler l'API



## <span name="license">License</span>

Le projet et son code peuvent être utilisés sans limitations.




